import time

start = time.time()

def collatz(num):  
    length = 1
    while num != 1:
        if num%2==0:
            num=num/2
            length+=1
        else:
            num=(num*3)+1
            length+=1
    return length

#print(collatz(100))
#print(collatz(837799))

maxlength=20
maxnum = 0

for i in range(2,1000000):
    cal=collatz(i)
    if cal>maxlength:
        maxlength=collatz(i)
        maxnum = i

print(maxnum,maxlength)
#print(collatz(maxnum))
    
end = time.time()

print(end-start)
